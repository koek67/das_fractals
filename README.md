# README #

## Overview ##
das_fractals is a 3D Lindenmayer System Fractal generator. A user can specify parameters for the L_System, then view it in real time in 3D. 

## Setup ##
In order to run this, you will need to download Eclipse and the Proclisping plug-in for it. This is a little bit of a pain so I will soon push a .jar file so that you can run the project without compiling the source. 

## Screenshot ##
![screenshot.jpg](https://bitbucket.org/repo/GAqpL5/images/2448836307-screenshot.jpg)